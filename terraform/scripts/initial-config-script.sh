#!/usr/bin/env bash

# Initial config script for hollenback.net. Install packages, etc.
# Eventually I'll split this in to separate files

# exit on any non-zero return
set -e -o pipefail

# version of pmwiki I'm installing
declare -r PMVER=2.2.115
declare -r HTMLDIR=/var/www/html
declare -r PMWIKIDIR=${HTMLDIR}/pmwiki
declare -ar PACKAGES=( screen unzip php )

# get packages upgraded and install
# some packages of our own
function RefreshAndInstallPackages {

  # refresh package list and do complete upgrade
  apt-get update
  apt-get -y upgrade

  # install our packages
  apt-get -y install ${PACKAGES[@]}

  # clean up packages we don't need any longer
  apt-get -y autoremove
}

# Install pmwiki and do some basic config
# At the end of this, the system will be running a basic pmwiki.
function DownloadAndInstallPmWiki
{
  cd /tmp
  wget http://www.pmwiki.org/pub/pmwiki/pmwiki-${PMVER}.zip
  unzip pmwiki-${PMVER}.zip -d ${HTMLDIR}
  rm /tmp/pmwiki-${PMVER}.zip
  ln -s ${HTMLDIR}/pmwiki-${PMVER} ${PMWIKIDIR}
  mkdir ${PMWIKIDIR}/wiki.d
  echo "<?php chdir('pmwiki'); include_once('pmwiki.php');" > ${HTMLDIR}/index.php
  rm ${HTMLDIR}/index*.html
  cp ${PMWIKIDIR}/docs/sample-config.php ${PMWIKIDIR}/local/config.php
  chown -R www-data: ${PMWIKIDIR}/
}

RefreshAndInstallPackages
DownloadAndInstallPmWiki
