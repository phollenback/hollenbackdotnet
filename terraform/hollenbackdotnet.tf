variable "sitename" {
    default = "hollenbackdotnet"
}

# pull aws creds from my standard aws cred file
provider "aws" {
  shared_credentials_file="~/.aws/credentials"
  region     = "us-east-1"
}

resource "aws_instance" "hollenbackdotnet" {
  # debian 9.9
  ami           = "ami-08e053c0dc7059edf"
  instance_type = "t2.micro"
  key_name      = "hollenbackdotnet"
  security_groups = [ "${var.sitename}" ]
  tags {
    Name = "${var.sitename}"
  }

  # file and remote-exec provisioners used to transfer an
  # initial config file to the new instance and then run it.
  provisioner "file" {
    source = "scripts/initial-config-script.sh"
    destination = "/home/admin/initial-config-script.sh"
    connection {
      # I need to change this in initial config
      user = "admin"
      private_key = "${file("${var.sitename}.pem")}"
      timeout = "60s"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/admin/initial-config-script.sh",
      "sudo /home/admin/initial-config-script.sh"
    ]
    connection {
      user = "admin"
      private_key = "${file("${var.sitename}.pem")}"
      timeout = "60s"
    }
  }
}

# Havent' decided if I need this yet, considering I won't
# be replacing website very often?
resource "aws_eip" "ip" {
  instance = "${aws_instance.hollenbackdotnet.id}"
}

resource "aws_security_group" "hollenbackdotnet" {
  name = "${var.sitename}"
  description = "${var.sitename} security group"

  # ssh access from anywhere
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # http access from anywhere
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # allow all outgoing connections
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}
